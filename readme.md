## Introduction
A demonstration of an alternative architecure for securing SPA applications, designed and proposed in the following
[article](https://leastprivilege.com/2019/01/18/an-alternative-way-to-secure-spas-with-asp-net-core-openid-connect-oauth-2-0-and-proxykit/), (Baier, D.). 

This small demo was implemented for use in a development environment to investigate alternatives to traditional SPA OAuth2 security models, see [here](https://tinyurl.com/y6xxrfw9).

The architecture uses an ASP.NET core server to provide:
- Authentication via OpenIDC Connect with IdentityServer4.
- Session management using samesite cookies.
- Reverse proxy for a private Flask API backend.

![Architecture](./src/Host/Angular/src/assets/images/architecture.png "Architecture")


## Project Structure 
The Visual Studio Code project structure is as follows:
- **src/Certs**: Contains self signed server certificate and root CA certificate.
- **src/Host**: ASP.NET Core MVC server application representing SPA gateway.
- **src/Host/Angular**: Angular 8 client application. Build output is located in
*src/Host/wwwroot*.
- **src/IdentityProvider**: ASP.NET MVC Server application representing IdentityServer4 server.
- **src/PythonApi**: Python Flask API server application that implemments a test
REST API. 
- **.vscode**: Contains build tasks, debug sessions and settings for Python 
virtual environment.


## Flask API 
The Flask API is location within the *src/PythonApi* folder.
Create a new [virtual environment](https://docs.python-guide.org/dev/virtualenvs/) and then issue the following commands:
```
pip install -r requirements.tx
python app.py
```
N.B the default virtual environment configured for a Visual Studio Code environment defaults to "${workspaceFolder}/src/PythonApi/.venv/bin/python"
Update this settings file if using Visual Studio Code and a virtual environment
has been configured other than ${workspaceFolder}/src/PythonApi/.venv

## Building Angular
The Angular source is located within the *src/Host/Angular* 
folder and is configured to build into *src/Host/wwwroot*.
When building the source, issue the following command:
```
ng build --delete-output-path
```
Alternatively, if using Visual Studio Code then use the preconfigured build task *build (Angular)*.

## Starting the Host MVC Server and IdentityProvider MVC Server Applications
Ensure that the package dependencies for the project are 
installed using *Visual Studio* IDE, or issue the following
command from within each project folder:
```
dotnet restore
```
Subsequently, build and run the server application, e.g.:
```
dotnet build
dotnet run
```

## Certificates
A self signed server certificate and CA root certificate is provided in 
*${workspaceFolder}/src/Certs*:
- A self signed PKCS12 (.pfx) server certificate has been provided in *src/Certs*. This will need to be trusted in local certificate store. If using a Linux environment then install *CA.crt* root certificate in trusted store, e.g. using
*update-ca-certificates*.
- A server certificate and RSA key has also been derived from the PKCS12 certificate and made available in **${workspaceFolder}/src/Certs* for use with
the Python Flask API.
- Ensure that the CA root is trusted by browser environment. 
- Ensure that browser request URL uses *localhost*, not 127.0.0.1.
- If using Firefox, then have to add exception for both host and Python API endpoints, otherwise a CORS options error will be received.


## Visual Studio Code Environment
The following build tasks are provided for Visual Studio Code:
- *build (All)*
- *build (Angular)*
- *build (Host)*
- *build (Identity Server)*
- *clean (All)*
- *clean (Host)*
- *clean (Identity Server)*

The following debug tasks are provided for Visual Studio Code:
- *Host (Web)*: Debug gateway MVC server application.
- *Flask (API)*: Debug Flask API server.
- *Full Stack*: Compound debug session, starts all server applications.
- *Identity Server (Seed Identity Users DB)*: Add two users, bob and alice to ASP.NET Identity DB with password Pass123$
- *Identity Server (Web)*: Debug IdentityServer4 server application.

The Python interpeter defaults to the following location:
- *${workspaceFolder}/src/PythonApi/.venv/bin/python*. This can be updated by
altering the *python.pythonPath* setting in 
${workspaceFolder}/.vscode/settings.json



## Visual Studio Code Multi-target Issue
- Visual Studio Code Full Stack debug starts all servers as a
compound launch task. Sometimes the IdentityServer is not started before MVC host client connects. 
- After a few seconds try https://localhost:5201 again and host connects. Is it possible to specify sequential dependencies of launch tasks?

## Future work
- Add Account Registration for specifying profile picture url, name, email etc.
