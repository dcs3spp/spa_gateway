﻿using IdentityModel.AspNetCore;
using IdentityModel.Config;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ProxyKit;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;

using Microsoft.AspNetCore.DataProtection;



namespace Host
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<HostConfigModel>(Configuration.GetSection("HostConfig"));
            HostConfigModel hostModel = Configuration.GetSection("HostConfig").Get<HostConfigModel>(); 

            services.AddProxy();

            services.AddMvcCore()
                .AddJsonFormatters()
                .AddAuthorization();

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = "cookies";
                options.DefaultChallengeScheme = "oidc";
            })
            .AddCookie("cookies", options =>
            {
                options.Cookie.Name = "bff";
                options.Cookie.SameSite = SameSiteMode.Strict;
            })
            .AddAutomaticTokenManagement()
            .AddOpenIdConnect("oidc", options =>
            {
                options.Authority = hostModel.IdentityProviderAuthority; //"https://demo.identityserver.io";
                options.ClientId = "server.hybrid";
                options.ClientSecret = "secret";
                
                options.ResponseType = "code id_token";
                options.GetClaimsFromUserInfoEndpoint = true;

                options.Scope.Clear();
                options.Scope.Add("api");
                options.Scope.Add("email");
                options.Scope.Add("offline_access");
                options.Scope.Add("openid");
                options.Scope.Add("profile");
                
                options.SaveTokens = true;

                options.TokenValidationParameters = new TokenValidationParameters
                {
                    NameClaimType = "name",
                    RoleClaimType = "role"
                };
            });

            //string homePath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            // services.AddDataProtection()
            //  .PersistKeysToFileSystem(new DirectoryInfo(homePath))
            //  .SetDefaultKeyLifetime(TimeSpan.FromDays(7));
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IOptions<HostConfigModel> options)
        {
            app.UseHttpsRedirection();
            
            app.UseDeveloperExceptionPage();
            
            app.UseMiddleware<StrictSameSiteExternalAuthenticationMiddleware>();
            app.UseAuthentication();

            app.Use(async (context, next) =>
            {
                if (!context.User.Identity.IsAuthenticated)
                {
                    await context.ChallengeAsync();
                    return;
                }

                await next();
            });

            app.Map("/api", api =>
            {
                api.RunProxy(async context =>
                {
                    var forwardContext = context.ForwardTo(options.Value.ApiBaseUrl+"/api");

                    var token = await context.GetTokenAsync("access_token");
                    forwardContext.UpstreamRequest.SetToken(token);

                    return await forwardContext.Execute();
                });
            });

            app.Map("/userinfo", userinfo =>
            {
                userinfo.RunProxy(async context =>
                {
                    var forwardContext = context.ForwardTo(options.Value.IdentityProviderBaseUrl+"/connect/userinfo");

                    var token = await context.GetTokenAsync("access_token");
                    forwardContext.UpstreamRequest.SetToken(token);

                    return await forwardContext.Execute();
                });
            });

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseMvc();
        }
    }
}