namespace IdentityModel.Config
{
    public class HostConfigModel
    {
        public string IdentityProviderAuthority { get; set; }

        public string IdentityProviderBaseUrl { get; set; }
        
        public string ApiBaseUrl { get; set; }
    }
}