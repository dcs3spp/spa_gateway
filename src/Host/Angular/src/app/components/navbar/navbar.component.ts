import { Breakpoints, BreakpointObserver }  from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit }    from '@angular/core';
import { LoginModel } from '../../core/models/login.model';
import { LoginManagerService } from '../../core/services/loginmanager.service';
import { Observable, Subject }   from 'rxjs';
import { map }          from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { ProfileDialogComponent } from '../../dialogs/profile/profile-dialog-component';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnDestroy, OnInit {

  private _ngUnsubscribe = new Subject();

  loginModel: LoginModel;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private loginService: LoginManagerService,
    private dialog: MatDialog) 
  { 
    loginService.loginInfo
    .pipe( takeUntil(this._ngUnsubscribe))
    .subscribe(
      (model: LoginModel) => { this.loginModel = model},
      (error) => { console.log(`loginService.userInfo`); }
    )
  }


  ngOnInit() {
  }

  ngOnDestroy() {
    this._ngUnsubscribe.next();
    this._ngUnsubscribe.complete();
  }

  logout(): void {
    this.loginService.logout();
  }

  openProfile(): void {
    this.dialog.open(ProfileDialogComponent, {
      width: '250px',
      data: this.loginModel
    });
  }

}
