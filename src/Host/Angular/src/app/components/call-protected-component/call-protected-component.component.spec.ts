import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallProtectedComponent } from './call-protected-component.component';

describe('CallProtectedComponentComponent', () => {
  let component: CallProtectedComponent;
  let fixture: ComponentFixture<CallProtectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallProtectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallProtectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
