import { Component, OnDestroy, OnInit } from '@angular/core';
import { APIProxyService } from '../../core/services/proxy.service';
import { APIService } from '../../core/services/api.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-call-protected-component',
  templateUrl: './call-protected-component.component.html',
  styleUrls: ['./call-protected-component.component.css']
})
export class CallProtectedComponent implements OnDestroy, OnInit {

  private _ngUnsubscribe = new Subject ();

  response: string;

  constructor(
    private proxyService: APIProxyService,
    private apiService: APIService
  ) { 
    this.response = ''; 
  }

  ngOnInit() {
    console.log("initialised CallProtectedComponent");
    this.callProxyProtected();
  }

  public callProxyProtected() : void {
    this.proxyService.getProtected()
    .pipe( takeUntil(this._ngUnsubscribe))
    .subscribe((result) => {
      this.response = result;
    }, (err) => {
      console.log(err);
      this.response = err;
    });
  }

  public callAPIProtected() : void {
    this.apiService.getProtected()
    .pipe( takeUntil(this._ngUnsubscribe))
    .subscribe((result) => {
      this.response = result;
    }, (err) => {
      console.log(err);
      this.response = err;
    });
  }

  ngOnDestroy(): void {
    this._ngUnsubscribe.next();
    this._ngUnsubscribe.complete();
  }

}
