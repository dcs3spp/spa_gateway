import { Component, OnDestroy, OnInit } from '@angular/core';
import { APIService } from '../../core/services/api.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-call-unprotected',
  templateUrl: './call-unprotected-component.component.html',
  styleUrls: ['./call-unprotected-component.component.css']
})
export class CallUnprotectedComponent implements OnDestroy, OnInit {

  private _ngUnsubscribe = new Subject ();

  response: string;

  constructor(private apiService: APIService) { 
    this.response = '';
  }

  ngOnInit() {
    this.apiService.getPing()
    .pipe( takeUntil(this._ngUnsubscribe))
    .subscribe((result) => {
      this.response = result;
    }, (err) => {
      console.log(err);
      this.response = err;
    });
  }

  ngOnDestroy(): void {
    this._ngUnsubscribe.next();
    this._ngUnsubscribe.complete();
  }

}
