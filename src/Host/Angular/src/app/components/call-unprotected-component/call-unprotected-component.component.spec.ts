import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallUnprotectedComponent } from './call-unprotected-component.component';

describe('CallUnprotectedComponent', () => {
  let component: CallUnprotectedComponent;
  let fixture: ComponentFixture<CallUnprotectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallUnprotectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallUnprotectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
