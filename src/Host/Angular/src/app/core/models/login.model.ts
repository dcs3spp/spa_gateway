import { UserInfo } from './userinfo.model';

export class LoginModel {
    private _loggedin : boolean;
    private _userinfo? : UserInfo;


    constructor() {
        this._loggedin = false;
        this._userinfo = undefined;
    }


    get loggedin() : boolean {
        return this._loggedin;
    }

    get userinfo() : UserInfo {
        return this._userinfo;
    }

    public login(value: UserInfo) {
        this._loggedin = true;
        this._userinfo = value;
    }

    public logout() {
        this._loggedin = false;
        this._userinfo = undefined;
    }
 }