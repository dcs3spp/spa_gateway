
/**
 * Model: UserInfo
 * 
 * Overview: OpenID Connnect UserInfo object obtained from /userinfo endpoint of gateway
 */
export class UserInfo {
   
    private _email: string;
    private _family_name: string;
    private _given_name: string;
    private _name: string;
    private _picture: string;
    private _preferred_username: string;
    private _website: string;
   

    constructor(email: string,
        familyName: string,
        givenName: string,
        name: string,
        picture: string,
        preferredUserName: string,
        website: string) {

        this.email = email;
        this.family_name = familyName;
        this.given_name = givenName;
        this.name = name;
        this._picture = picture;
        this.preferred_username = preferredUserName;
        this.website = website;
    }

    get email(): string {
        return this._email;
    }

    set email(value: string) {
        this._email = value;
    }

    get picture(): string {
        return this._picture;
    }

    set picture(value: string) {
        this._picture = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get given_name(): string {
        return this._given_name;
    }

    set given_name(value: string) {
        this._given_name = value;
    }

    get family_name(): string {
        return this._family_name;
    }

    set family_name(value: string) {
        this._family_name = value;
    }

    get website(): string {
        return this._website;
    }

    set website(value: string) {
        this._website = value;
    }

    get preferred_username() {
        return this._preferred_username;
    }

    set preferred_username(value: string) {
        this._preferred_username = value;
    }

}

