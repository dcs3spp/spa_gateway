import { Injectable, OnDestroy, OnInit } from '@angular/core';
import { ReplaySubject, Observable, Subject } from 'rxjs';
import { LoginModel } from '../models/login.model';
import { UserInfo } from '../models/userinfo.model';
import { HostService } from './host.service';
import { takeUntil } from 'rxjs/operators';
import { UserService } from '../services/user.service';



// inject provider root since recommended way
@Injectable({providedIn: 'root'})
export class LoginManagerService implements OnDestroy, OnInit {

    private _loginModel;
    private _loginSubj : ReplaySubject<LoginModel>;
    private _ngUnsubscribe = new Subject();

    loginInfo: Observable<LoginModel>;


    constructor(private userService: UserService, private hostService: HostService) {
        this._loginModel = new LoginModel();
        this._loginSubj = new ReplaySubject<LoginModel>(this._loginModel);
        this.loginInfo = this._loginSubj.asObservable();

        this.connect();
    }

    
    ngOnInit() {

    }

    ngOnDestroy() {
        this._ngUnsubscribe.next();
        this._ngUnsubscribe.complete();
    }


    public connect() : Observable<UserInfo> {
        let obs = this.userService.getUserInfo();

        obs
        .pipe(takeUntil(this._ngUnsubscribe))
        .subscribe(
            (result : UserInfo) => { 
                this._loginModel.login(result);
                this._loginSubj.next(this._loginModel);
            },
            (error) => { console.log(`UserService:getUserInfo():error => ${error}`); }
        );

        return obs;
    }

    public logout() {
        this.hostService.logout();
        this._loginModel.logout();
        this._loginSubj.next(this._loginModel);
    }
}
