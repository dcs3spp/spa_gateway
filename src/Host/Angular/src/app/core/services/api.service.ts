import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';


const endpoint = 'https://localhost:4250/api/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};


@Injectable({
  providedIn: 'root'
})
export class APIService {

  constructor(private http: HttpClient) { }

  getIndex(): Observable<string> {
    return this.http.get<string>(endpoint, httpOptions);
  }

  getProtected(): Observable<string> {
    return this.http.get<string>(endpoint + 'protected', httpOptions);
  }

  getPing(): Observable<string> {
    return this.http.get<string>(endpoint + 'ping', httpOptions);
  }


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      console.error(error); 
  
      console.log(`${operation} failed: ${error.message}`);
  
      return of(result as T);
    };
  }
}
