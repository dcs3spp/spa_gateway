import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UserInfo } from '../models/userinfo.model';

const endpoint = 'https://localhost:5001/userinfo';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({providedIn: 'root'})
export class UserService {

    constructor(private http: HttpClient) {
     
    }

    public getUserInfo(): Observable<UserInfo> {
        return this.http.get<UserInfo>(endpoint, httpOptions)
        .pipe(
          catchError(this.handleError<UserInfo>('getUserInfo'))
        );
    }

    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            
            console.error(error); 
            console.log(`${operation} failed: ${error.message}`);
            return of(result as T);
        };
    }
}
