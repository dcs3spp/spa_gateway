import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { WINDOW } from "./window.service";

const endpoint = 'https://localhost:5001/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class HostService {

  constructor(private http: HttpClient, @Inject(WINDOW) private window: Window) { }

  getUser(): Observable<string> {
    return this.http.get<string>(endpoint + 'user/info', httpOptions);
  }

  getUserInfo(): Observable<string> {
    return this.http.get<string>(endpoint + 'userinfo', httpOptions);
  }

  logout(): void { 
    this.window.location.href = endpoint + 'user/logout';
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
