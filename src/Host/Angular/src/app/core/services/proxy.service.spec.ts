import { TestBed } from '@angular/core/testing';

import { APIProxyService } from './proxy.service';

describe('RestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: APIProxyService = TestBed.get(APIProxyService);
    expect(service).toBeTruthy();
  });
});
