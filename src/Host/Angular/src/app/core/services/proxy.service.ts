import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';


const endpoint = 'https://localhost:5001/api/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  }),
  withCredentials: true
};


@Injectable({
  providedIn: 'root'
})
export class APIProxyService {
  
  constructor(private http: HttpClient) { }


  getIndex(): Observable<string> {
    return this.http.get<string>(endpoint, httpOptions);
  }

  getProtected(): Observable<string> {
    return this.http.get<string>(endpoint + 'protected', httpOptions);
  }

  getPing(): Observable<string> {
    return this.http.get<string>(endpoint + 'ping', httpOptions);
  }


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
