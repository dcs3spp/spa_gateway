import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {LoginModel} from '../../core/models/login.model';

@Component({
  selector: 'profile-dialog',
  templateUrl: 'profile-dialog-component.html',
})
export class ProfileDialogComponent implements OnInit {

  form: FormGroup;

  constructor(
    public fb: FormBuilder,
    public dialogRef: MatDialogRef<ProfileDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: LoginModel) {}


  ngOnInit() {
    this.form = this.fb.group({
      family_name: [{value: this.data.userinfo.family_name, disabled: true}, []],
      given_name: [{value: this.data.userinfo.given_name, disabled: true}, []],
      name: [{value: this.data.userinfo.name, disabled: true}, []],
      picture: [{value: this.data.userinfo.picture, disabled:true}, []],
      preferred_username: [{value: this.data.userinfo.preferred_username, disabled:true}, []],
      email: [{value: this.data.userinfo.email, disabled:true}],
      website: [{value: this.data.userinfo.website, disabled:true}, []]
    });
  }


  close(): void {
    this.dialogRef.close();
  }


}
