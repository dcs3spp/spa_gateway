import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CallProtectedComponent } from './components/call-protected-component/call-protected-component.component';
import { CallUnprotectedComponent } from './components/call-unprotected-component/call-unprotected-component.component';
import { HomeComponent } from './components/home-component/home-component.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'protected', component: CallProtectedComponent },
  { path: 'unprotected', component: CallUnprotectedComponent },
  { path: '**', redirectTo: '/heroes', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
