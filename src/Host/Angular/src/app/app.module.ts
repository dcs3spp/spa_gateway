import { NgModule } from '@angular/core';

import { AppComponent } from './components/app/app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CallProtectedComponent } from './components/call-protected-component/call-protected-component.component';
import { CallUnprotectedComponent } from './components/call-unprotected-component/call-unprotected-component.component';
import { CoreModule } from './core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LayoutModule } from '@angular/cdk/layout';
import { MaterialModule } from './modules/material.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home-component/home-component.component';
import { ProfileDialogComponent } from './dialogs/profile/profile-dialog-component';


@NgModule({
  declarations: [
    AppComponent,
    CallProtectedComponent,
    CallUnprotectedComponent,
    HomeComponent,
    NavbarComponent,
    ProfileDialogComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    CoreModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    LayoutModule,   
    ReactiveFormsModule
  ],
  providers: [],
  entryComponents: [ProfileDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
