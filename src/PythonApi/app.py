import os
import socket 

from flask import Flask
from flask import jsonify
from flask import request
from flask import send_from_directory
from flask_cors import CORS
from flask_oidc import OpenIDConnect


app = Flask(__name__)
app.config.from_pyfile('config.cfg')
cors = CORS(app, resources={
    r"/api/*": {
        "origins": "https://localhost:5001",
        "supports_credentials": "true"
    }
})
oidc = OpenIDConnect(app)

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/api')
def index():
    return jsonify(response='Index Page')

@app.route('/api/ping', methods=['GET'])
def ping():
    return jsonify(response='pong')

@app.route('/api/protected', methods=['GET'])
@oidc.accept_token(require_token=True, scopes_required=['api'])
def protected():
    return jsonify(response='protected')


if __name__ == '__main__':
    app.run(
        ssl_context=('../Certs/server.crt','../Certs/server.key'), 
        debug=True, host='127.0.0.1', port=4250
    )
