#!/bin/bash

#
# Run this script from within ${workspaceFolder}/src/IdentityProvider
#
rm -rf Migrations

# create migrations for grants
dotnet ef migrations add PersistedGrantDb \
  --context PersistedGrantDbContext \
  --output-dir Migrations/PeristedGrantDb \
  -s Server \
  --project DataAccess

# create migrations for clients, client secrets etc
dotnet ef migrations add ConfigurationDb \
  --context ConfigurationDbContext \
  --output-dir Migrations/ConfigurationDb \
  -s Server \
  --project DataAccess

# creation migrations for ASP.NET Core identity
dotnet ef migrations add IdentityUsers \
  --context ApplicationDbContext \
  --output-dir Migrations/IdentityUsers \
  -s Server \
  --project DataAccess

# will this get created in same database if use same connection string
# dotnet ef database update command with ApplicationDbContext only
# created the ASP.NET Core Identity tables.