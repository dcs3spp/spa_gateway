﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess
{
    public class ApplicationUser : IdentityUser
    {
    }
}
