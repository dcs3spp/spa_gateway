﻿using System.Collections.Generic;
using IdentityServer4.Models;
using static IdentityModel.OidcConstants;

namespace DataAccess
{
    public class Config
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email()
            };
        }

        // scopes define the API resources in your system
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("api", "api")
                {
                    ApiSecrets = { new Secret("secret".Sha256()) }
                }
            };
        }

        // client want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients()
        {
            // client credentials client
            return new List<Client>
            {
                new Client
                {
                    ClientId = "server.hybrid",
                    AllowedGrantTypes = IdentityServer4.Models.GrantTypes.Hybrid,
                    AllowOfflineAccess = true,
                    AllowedScopes = 
                    { 
                        StandardScopes.Email, 
                        StandardScopes.OpenId, 
                        StandardScopes.Profile,
                        StandardScopes.OfflineAccess,
                        "api"
                    },
                    AllowedCorsOrigins = 
                    {
                        "https://localhost:5001"
                    },
                    ClientSecrets = 
                    {
                        new Secret("secret".Sha256())
                    },
                    //LogoutUri = "http://localhost:5001/signout-oidc",
                    //PostLogoutRedirectUris = { "https://localhost:5001/signout-oidc" },
                    RedirectUris = { "https://localhost:5001/signin-oidc" },
                    RefreshTokenUsage = TokenUsage.ReUse
                }
            };
        }
    }
}
